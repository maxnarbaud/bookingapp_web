# Booking Web App - ASP.NET (GitLab CI/CD edition)
⚠️ **This project is for educational purposes, and is not intended for a real production environment.**

## Table of contents
1. [Home Page Preview](#home-page-preview)
2. [Features](#features)
3. [Local development](#local-development-for-windows)
    1. [With Docker](#with-docker-in-vs-code)
    2. [Without Docker](#without-docker-support-hot-reload)
4. [Add External Login/Register](#add-external-loginregister)
5. [Deployment with CI/CD](#deployment-with-cicd)
6. [Built With](#built-with)
7. [Authors](#authors)
8. [License](#license)

## Home Page Preview
![Home Page Screenshot](https://raw.githubusercontent.com/maximenrb/BookingApp_ASP.NET/master/git_img/screencapture-home_page.png)

## Features
- **Home page:** research offers, view last offers
- **Login & Register:** external authentification (Google), 2FA authentication
- **CRUD** (create, read, update, delete and search):
  - Accommodations 
  - Offers
  - Bookings (no update and deletion)
  - Users (for Administrator only) 
- **Create/Update forms:**
  - Accommodation:
    - General informations, house rules, manage pictures
    - Manage rooms and amenities 
  - Offer
- **Booking page** with accommodation and offer details
- **Offers bookmark**
- **E-Wallet** with transactions

## Local development (for Windows)
Firstly, install [.NET SDK](https://dotnet.microsoft.com/en-us/download)

### With Docker (in VS Code)
1. Download & Install [Docker extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) in the IDE
2. Download & Install [Docker Desktop](https://www.docker.com/products/docker-desktop/): this can be done automatly by going to the Docker tab
3. Launch Docker Desktop
4. Enter ` docker-compose build ` in terminal to create docker images
5. Enter ` docker-compose up -d ` to start containers

You can access with this URL: https://localhost:8001/

### Without Docker (support hot reload)

Installation of HTTPS certificate *[(See for more informations)](https://github.com/dotnet/dotnet-docker/blob/main/samples/host-aspnetcore-https.md)* :

1. Create an auto-signed certificat. In terminal enter: ` dotnet dev-certs https -ep .\https\aspnetapp.pfx -p crypticpassword `
2. Install the certificat: ` dotnet dev-certs https --trust `

#### Visual Studio Code (or others tools)
In a terminal (in project directory):

3. Install Entity Framework tool: ` dotnet tool install --global dotnet-ef `
4. Create the database: ` dotnet ef database update `
5. Launch the project: ` dotnet watch run --launch-profile BookingApp `

#### Microsoft Visual Studio
3. Create the dabatase (with the help of ` Migrations ` directory) with Entity Framework :

    Go in ` Tools > NuGet Package Manager > Package Manager Console `, and enter:
    ```powershell
    Update-Database
    ```

4. Launch the project with the interface


**⚠️ [IMPORTANT]** After launching the project with any mode, go to ` <your url>/InitDB ` *(ex: ` https://localhost:44369/InitDB ` )* in order to create Roles and default Users in database

The following default users are created :

| Type  | Email          | Password |
| ----- | -------------- | -------- |
| User  | user@user.fr   | 123aA_   |
| Host  | host@host.fr   | 123aA_   |
| Admin | admin@admin.fr | 123aA_   |

## Add External Login/Register
Firstly, you must enable secret storage, for this open .NET Core CLI in the project directory, go in ` View > Terminal ` or use ` Ctrl+ù `.

Then, run the following command : 
```powershell
dotnet user-secrets init 
```

#### Google ([See documentation for setup](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/google-logins?view=aspnetcore-5.0)) :
* Store the Google client ID and secret :
    
    ```powershell
    dotnet user-secrets set "Authentication:Google:ClientId" "<client-id>"
    dotnet user-secrets set "Authentication:Google:ClientSecret" "<client-secret>"
    ```

## Deployment with CI/CD
You must have a working Kubernetes cluster with Traefik installed. Traefik must manage the SSL certificate with the DNS-01 challenge (in order to have a valid wildcard certificate for review environments).
1. Deploy a GitLab agent in your cluster: [Register the agent with GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab)

2. Create some GitLab variables:

| Key                    | Value                                 | Options   | Environments  |
| ---------------------- | ------------------------------------- | --------- | ------------- |
| HOST_URL               | *Your root domain*                    |           | All (default) |
| KUBE_CONTEXT           | *path/to/agent/repository:agent-name* |           | All (default) |
| ASPNETCORE_ENVIRONMENT | Production                            |           | prod          |
| ASPNETCORE_ENVIRONMENT | Development                           |           | dev           |
| DATABASE_PASSWORD      | *Strong radom password 1*             | Protected | prod          |
| DATABASE_PASSWORD      | *Strong radom password 2*             |           | dev           |
| DATABASE_PASSWORD      | *Strong radom password 3*             |           | All (default) |

> It's also recommend to use private GitLab runners in order to protect secrets from beeing exposed in public runners: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

You will have 3 types of environments : production, development and multiple reviews for each branch. **In order to deploy a new production version, you must increment version in ` version.txt `.**

## Built With
- [ASP.NET Core MVC](https://docs.microsoft.com/en-us/aspnet/core/mvc/overview?view=aspnetcore-5.0) - ASP.NET Core MVC is a rich framework for building web apps and APIs using the Model-View-Controller design pattern.
- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) - Entity Framework Core is a modern object-database mapper for .NET. It supports LINQ queries, change tracking, updates, and schema migrations. EF Core works with many databases, including SQL Database (on-premises and Azure), SQLite, MySQL, PostgreSQL, and Azure Cosmos DB.
- [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) - Microsoft SQL Server is a relational database management system developed by Microsoft.
- [Bootstrap](https://getbootstrap.com/) - Bootstrap is a free and open-source CSS framework directed at responsive, designed for front-end web development.
- [Docker](https://www.docker.com/) - Docker is an open source platform that enables developers to build, deploy, run, update and manage containers.
- [Kubernetes](https://kubernetes.io/) - Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications.
- [Traefik](https://traefik.io/traefik/) - Traefik is an open-source reverse proxy  and load balancer written in Go, that makes deploying services easy.

## Authors
- [MaximeNrb](https://github.com/maximenrb)

## License
<img align="left" width= "150" alt="CC Attribution-NonCommercial-NoDerivatives 4.0 International Public License Logo" src="https://raw.githubusercontent.com/maximenrb/BookingApp_ASP.NET/master/git_img/by-nc-nd.eu.png">

This project is licensed under the CC Attribution-NonCommercial-NoDerivatives 4.0 International Public License

See the [LICENSE.md](https://github.com/maximenrb/BookingApp_ASP.NET/blob/master/LICENSE.md) file for details
