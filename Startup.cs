using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using BookingApp.Data;
using BookingApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

        if (Environment.GetEnvironmentVariable("DATABASE_SOURCE") != null) 
        {
            builder.DataSource = Environment.GetEnvironmentVariable("DATABASE_SOURCE");
            builder.UserID = Environment.GetEnvironmentVariable("DATABASE_USER");
            builder.Password = Environment.GetEnvironmentVariable("DATABASE_PASSWORD");
        } 
        else 
        {
            builder.DataSource = "(localdb)\\mssqllocaldb";
        }

        builder.InitialCatalog = "airbnbDB";
        builder.MultipleActiveResultSets = true;
        builder.TrustServerCertificate = true;

        services.AddDbContext<AppContextDB>(options => options.UseSqlServer(builder.ConnectionString));

        services
            .AddDefaultIdentity<User>()
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<AppContextDB>();

        // FIXME: REACTIVATE !!! (Error in Docker)
        // services.AddAuthentication()
        //     .AddGoogle(options =>
        //     {
        //         IConfigurationSection googleAuthNSection = Configuration.GetSection("Authentication:Google");

        //         options.ClientId = googleAuthNSection["ClientId"];
        //         options.ClientSecret = googleAuthNSection["ClientSecret"];
        //     });

        services.Configure<SecurityStampValidatorOptions>(o => o.ValidationInterval = TimeSpan.FromSeconds(0));

        services.AddControllersWithViews();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void ConfigureApp(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // TODO: Find a solution to not do this at every launch
        using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
        {
            var context = serviceScope.ServiceProvider.GetRequiredService<AppContextDB>();
            context.Database.Migrate();
        }

        if (!env.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }
        
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
            endpoints.MapRazorPages();
        });
    }
}
