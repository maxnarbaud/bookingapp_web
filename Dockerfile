# Get ENV argument
ARG RUNTIME_ENV

# Use Microsoft's official build .NET image: https://hub.docker.com/_/microsoft-dotnet-core-sdk/
FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /app

# Install production dependencies.
# Copy csproj and restore as distinct layers.
COPY *.csproj ./
RUN dotnet restore

# Copy project code to the container image.
COPY . ./
WORKDIR /app

# Build a release artifact.
RUN dotnet publish -c Release -o out

# Use Microsoft's official runtime .NET image: https://hub.docker.com/_/microsoft-dotnet-core-aspnet/
FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine-amd64 AS base-runtime
WORKDIR /app
COPY --from=build /app/out ./

RUN apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

FROM base-runtime AS runtimelocal
ENV ASPNETCORE_URLS="https://+;http://+"
ENV ASPNETCORE_HTTPS_PORT=8001

FROM base-runtime AS runtime
ENV ASPNETCORE_URLS="http://+:8080"

FROM runtime${RUNTIME_ENV} AS final-runtime
# Run the web service on container startup.
ENTRYPOINT ["dotnet", "BookingApp.dll"]
